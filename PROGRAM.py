"""
@author : magnus.ersdal@uib.no
"""
import fire

import multiprocessing as mp

import run_jtag_svf_programming as jp

def program_all_RUs(*cfgs):
    jobs = []
    for cfg in cfgs:
        #run individual python exec.
        p = mp.Process(target=jp.configure_PA3, args=(cfg,))
        jobs.append(p)
        p.start()

if __name__ == "__main__":
    print("The input to this script is one or several configuration files")
    fire.Fire(program_all_RUs)
