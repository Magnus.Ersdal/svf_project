# -*- coding: utf-8 -*-
"""
Created on Fri Mar 16 13:51:48 2018

@author: magnus rentsch ersdal
@email : magnus.ersdal@uib.no

usage:
import SCARegs as scregs
from SCARegs import findreg
SCARegcmds = scregs.SCARegcmds # only if you want
SCARegdefaults = scregs.SCARegdefaults # only if you want
SCARegs = scregs.SCARegs ## "global" SCA-regs


examplenum = SCARegs['CTRL_W_CRB']

findreg('CTRL')

"""



"""register commands from the SCA manual."""
SCARegs = {
    # SCA command registers
    'CTRL_R_ID': 0x140000D1,  # this is SCA V2
    'CTRL_W_CRB': 0x00000002,
    'CTRL_R_CRB': 0x00000003,
    'CTRL_W_CRC': 0x00000004,
    'CTRL_R_CRC': 0x00000005,
    'CTRL_W_CRD': 0x00000006,
    'CTRL_R_CRD': 0x00000007,
    'CTRL_R_SEU': 0x130000F1,
    'CTRL_C_SEU': 0x130000F1,

    # JTAG COMMANDS
    'JTAG_W_CTRL': 0x13000080,
    'JTAG_R_CTRL': 0x13000081,
    'JTAG_W_FREQ': 0x13000090,
    'JTAG_R_FREQ': 0x13000091,
    'JTAG_W_TDO0': 0x13000000,
    'JTAG_R_TDI0': 0x13000001,
    'JTAG_W_TDO1': 0x13000010,
    'JTAG_R_TDI1': 0x13000011,
    'JTAG_W_TDO2': 0x13000020,
    'JTAG_R_TDI2': 0x13000021,
    'JTAG_W_TDO3': 0x13000030,
    'JTAG_R_TDI3': 0x13000031,
    'JTAG_W_TMS0': 0x13000040,
    'JTAG_R_TMS0': 0x13000041,
    'JTAG_W_TMS1': 0x13000050,
    'JTAG_R_TMS1': 0x13000051,
    'JTAG_W_TMS2': 0x13000060,
    'JTAG_R_TMS2': 0x13000061,
    'JTAG_W_TMS3': 0x13000070,
    'JTAG_R_TMS3': 0x13000071,
    'JTAG_ARESET': 0x130000C0,
    'JTAG_GO': 0x130000A2,
    'JTAG_GO_M': 0x130000B0

}
"""commands that follow the registers"""
SCARegcmds = {
    'CTRL_R_ID': 0x1,  # this is SCA V2
    'CTRL_C_SEU': 0x0
}
"""register defaults, just in case"""
SCARegdefaults = {
    'JTAG_W_CTRL': 0x1000,
}


class trid():
    def __init__(self):
        return None


# helper function.
import re  # regexp


def findreg(searchterm):
    for key in SCARegs.keys():
        if re.search(searchterm, key):
            print(searchterm, "found in **", key, "** value:",
                  SCARegs[key], "/", "0x"+format(SCARegs[key], '08x'))
            if key in SCARegdefaults:
                print("    ", "|--", key, "default val:",
                      SCARegdefaults[key], "/", "0x"+format(SCARegdefaults[key], '08x'))
            if key in SCARegcmds:
                print("    ", "|--", key, "also has following command attached:",
                      SCARegcmds[key], "/", "0x"+format(SCARegcmds[key], '08x'))
    return None
