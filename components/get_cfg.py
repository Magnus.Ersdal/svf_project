"""
author = magnus.ersdal@uib.no
"""
import fire


def get_configuration_variables(filename):
    #filename = "defaults.cfg"
    with open(filename,'r') as cfgfile:
        import sys
        if sys.version_info >= (3,0):
            # we are in python 3xx 
            import configparser as cp
            config = cp.ConfigParser()
            config.read_file(cfgfile)
        else:
            # python2 :(
            print("Python 2. Upgrade required by 2020")
            import ConfigParser as cp
            config = cp.ConfigParser()
            config.readfp(cfgfile)

# hardcoded option names, because we need the options to run the tool.
# more specifically, to initialize the Sca object.
    sect = ['CRU']*2 + ['RU'] + ['FILE']
    opt = ['id_card','board','gbt_ch','svf_filename']
    variables = {}

    print("cfg file read")
    # abs minimum cfg options...
    try:
        for section, option in zip(sect,opt):
            print(section,option, config.get(section,option))
            variables[option] = config.get(section,option)
    except (NameError, cp.NoOptionError) as e:
        print("Name/NoOptionError !, name \'{}\' is not defined.\
     Please check the config file".format(option))
        print (e.message)
        raise
    
    # get any items in JTAG section:
    if config.has_section('JTAG'):
        for option, val in config.items('JTAG'):
            variables[option] = val
            print("got JTAG variable \'{}\' = {}".format(option, val))

    print("cfg dict:")
    print(variables)
    return variables

if __name__ == '__main__':
    fire.Fire(get_configuration_variables)
