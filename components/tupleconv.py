# -*- coding: utf-8 -*-
"""
Created on Wed May  2 15:57:55 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
"""

def foo2(tuples):
    '''make a list of tuples of (int, length) into a list of 32 bit integers [1,2,3]'''
#    binarylist = "".join([np.binary_repr(a, b) for a, b in inp]) # bin(a)[2:].rjust(b, '0')
    binarylist = "".join([bin(a)[2:].rjust(b, '0') for a, b in tuples])
    totallength = len(binarylist)
    tot32 = int(totallength/32)
    i32list = [int(binarylist[i:i+32],2) for i in range(0, tot32*32, 32)]
    print([hex(a) for a in i32list])
    remlen = totallength - tot32*32
    remint = int(binarylist[-remlen:],2)
    return i32list, remint, remlen


def tpli_2_32ili(tuples):  
    liol = []
    remn = 0
    remlen = 0
    for num, size in tuples:
        num = (remn << size) | num
        size = size + remlen
        remn = 0
        remlen = 0
      
        if size > 32:
            n32 = int(size/32)
            remlen = size - (n32*32)
            if remlen:
                remn = num & int("1"*remlen,2)
            tnum = num >> remlen
            
            for i in reversed(range(n32)):
                liol.append(tnum >> (i*32) & 0xffffffff)
                
        else:
            remn = num
            remlen = size
    return liol, remn, remlen

def listof32s_append_tuplelist(liol,tuples):
    #print(tuples)
    #print("len = ", sum([x[1] for x in tuples]))
    tuples = list(filter(lambda x: x != (0,0), tuples))
    binarylist = "".join([bin(a)[2:].rjust(b, '0') for a, b in tuples])
    totallength = len(binarylist)
    #print("totallength=",totallength)
    tot32 = int(totallength/32)
    liol += [int(binarylist[i:i+32][::-1],2) for i in range(0, tot32*32, 32)]
    remlen = totallength - tot32*32
    #print("wrote",tot32,"32bit numbers")
    #print("remlen is:",remlen)
    #remint = int(binarylist[-remlen:],2) # this might be WRONG
    if remlen:
        remint = int(binarylist[tot32*32:],2)    
    else:
        remint = 0    
    tuples = [(remint,remlen)]
    #print("gave")
    #print([hex(x) for x in liol])
    #print("returns:", tuples)
    return tuples


def listof32s_append_tuplelist_old(liol,tuples):
    """make a list (liol) of 32 bit integers (implicit), from the explicit tuples [(number,binlength)]"""
    # bug: accepts (0,0) and actually makes it into something :(
    # lacking feature: reverse the int. 
    remn = 0
    remlen = 0
    print(tuples)
    for num, size in tuples:
        if False: # change from PASS to
            remn = remn
            remlen = remlen
        else:
            try:
                num = (remn << size) | num
                #num = (remn << size) | num # this is maybe not what we wanted.
                #num = (num << remlen) | remn # looked nicer, but no.
                #num = (num << size) | remn # definetly not
            except ValueError:
                print("ValueError in tupleconv")
                print("num, remn, size")
                print(num, remn, size)
                print("tuples:")
                print(tuples)
            size = size + remlen
            remn = 0
            remlen = 0
          
            if size > 32:
                n32 = int(size/32)
                remlen = size - (n32*32)
                if remlen:
                    remn = num & int("1"*remlen,2)
                tnum = num >> remlen
                
                for i in reversed(range(n32)):
                    liol.append(tnum >> (i*32) & 0xffffffff)
                    
            else:
                remn = num
                remlen = size
    if remlen:
        tuples = [(remn,remlen)]
    else:
        tuples = []
    
    print("gave")
    print([hex(x) for x in liol])
    return tuples
#    return liol, remn, remlen


if __name__== '__main__':
    import cProfile
    import pstats
    foo = tpli_2_32ili
#    tv = [(0,128),(1,12),(0,32),(0xFF,8),(1,32),(1,32)]
#    tv = [(0,32),(0xff,8),(1,32)]
#    tv = [(0xff,128),(1,12),(0,32),(0xFF,8),(2,32),(5,32)]
    tv = [(2, 5), (2, 3), (0, 7), (2, 2), (2, 3), (0, 7), (2, 2), (2, 3), (0, 7), (2, 2), (2, 3), (0, 7), (2, 2), (2, 3), (0, 7), (2, 2), (2, 3), (0, 7), (2, 2), (2, 3), (0, 7), (2, 2), (2, 3), (0, 7), (2, 2), (2, 3), (0, 7), (2, 2), (2, 3), (0, 7), (2, 2), (2, 3), (0, 7), (2, 2)]
#    tv = [(1,12),(0,32)]
    listof32s_append_tuplelist([],tv)
    print("test:",foo(tv))
    print("ref: ",foo2(tv))
    #cProfile.run('for i in range(50000): foo(tv)',sort='tottime')
