import sys
import time
from time import sleep

import libReadoutCard

class Roc:
    def openROC(self, pcie_id, bar_ch, debug = None):
        if debug == None :
            self.ch = libReadoutCard.BarChannel(pcie_id, bar_ch)
        else :
            print('Open ROC')
    
    def rocWr(self, reg, data, debug = None) :
        if debug == None :
            self.ch.register_write(reg, data)
        else :
            print('SCA WR ', hex(data))

    def rocRd(self, reg, debug = None) :
        if debug == None :
            return self.ch.register_read(reg)
        else :
            return 0
