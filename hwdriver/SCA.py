import sys
import time
from time import sleep

import hwdriver.ROC
from hwdriver.ROC import Roc

import hwdriver.UTILS
from hwdriver.UTILS import Utils

class Sca (Utils, Roc):
    def __init__(self, pcie_id, bar_ch, gbt_ch, board, debug = None):
        """
        Class constructor. Init the addresses and the file name
        """
        self.openROC(pcie_id, bar_ch, debug)      
        
        if gbt_ch == 0 :
            if board == 'CRU' : 
                self.base_add = 0x04224000
            else :
                self.base_add = 0x1c0
        elif gbt_ch == 1:
            self.base_add = 0x04244000
        elif gbt_ch == 2:
            self.base_add = 0x04264000
        elif gbt_ch == 3:
            self.base_add = 0x04284000
        else :
            self.base_add = 0x04224000
        
        self.wr_add_data = self.base_add + 0x20
        self.wr_add_cmd = self.base_add + 0x24
        self.wr_add_ctr = self.base_add + 0x28

        self.rd_add_data = self.base_add + 0x30
        self.rd_add_cmd = self.base_add + 0x34
        self.rd_add_ctr = self.base_add + 0x38
        self.rd_add_mon = self.base_add + 0x3c

        self.trid = 0x0
        self.cmd = 0x0

        self.file_name = 'tpc_cmds'
        
    #--------------------------------------------------------------------------------
    def wr(self, cmd, data, trid = None, debug = None):
        """
        Write the 64 bit (data + cmd)) to the SCA interface and execute the command
        If the TRID is not defined, it increments it for every wr
        """
        if trid is None : 
            self.trid = self.trid + 1
            if (self.trid == 0xff):
                self.trid = 0x1
            self.cmd = cmd & 0xff00ffff
            self.cmd = self.cmd + (self.trid << 16)        
        else : 
            self.cmd = cmd & 0xff00ffff
            self.cmd = self.cmd + (trid << 16)        
                
        self.rocWr(self.wr_add_data, data, debug)
        self.rocWr(self.wr_add_cmd, self.cmd, debug)
        self.exe(debug)
        
        time = self.rocRd(self.rd_add_mon, debug)
        time = time & 0xffff
#        print('WR - DATA %10s CH %4s TR %4s CMD %4s TIME %d' % (hex(data), hex(self.cmd >> 24),  hex((self.cmd >> 16) & 0xff), hex(self.cmd & 0xff), time))
    #--------------------------------------------------------------------------------
    def wr_fast(self, cmd, data, trid):
        self.cmd = (cmd & 0xff00ffff) | (trid << 16)
        
        self.rocWr(self.wr_add_data, data)
        self.rocWr(self.wr_add_cmd, self.cmd)
        
        self.rocWr(self.wr_add_ctr, 0x4)
        self.rocWr(self.wr_add_ctr, 0x0)
        
        self.waitBusy()
    #--------------------------------------------------------------------------------
    
    #--------------------------------------------------------------------------------
    def rd(self, debug = None) :
        """
        Read the feedback from the SCA component
        """
        err_int = 64
        while (err_int != 0) :
            data = self.rocRd(self.rd_add_data, debug)
            cmd = self.rocRd(self.rd_add_cmd, debug)
            ctrl = self.rocRd(self.rd_add_ctr, debug)

            err_code = hex(cmd & 0xff)    
            err_int = int(err_code,0)
        
            if err_int == 64 :
                # CH BUSY
                time.sleep(1.0/1000.0)
            elif err_int != 0 :
                # ERROR
                self.error(err_code)
            else :
                pass
                #print('RD - DATA %10s CH %4s TR %4s ERR %4s CTRL %4s' % (hex(data), hex(cmd >> 24), hex((cmd >> 16) & 0xff), hex(cmd & 0xff), hex(ctrl)))
    #--------------------------------------------------------------------------------

    #--------------------------------------------------------------------------------
    def exe(self,debug = None) :
        """
        Trigger the execution of the SCA command
        """
        self.rocWr(self.wr_add_ctr, 0x4, debug)
        self.rocWr(self.wr_add_ctr, 0x0, debug)
        self.waitBusy(debug)
    #--------------------------------------------------------------------------------

    #--------------------------------------------------------------------------------    
    def waitBusy(self,debug = None) :
        """
        Wait for the SCA component to be available
        """
        busy_cnt = 0
        busy = 0x1
        
        while (busy == 0x1) :
            busy = self.rocRd(self.rd_add_ctr, debug)
            busy = busy >> 31
            busy_cnt = busy_cnt + 1
            if busy_cnt == 1e6:
                print('CHIP is stuck ... exiting')
                sys.exit()
    #--------------------------------------------------------------------------------

    #--------------------------------------------------------------------------------       
    def init(self, debug = None) :
        """
        Init the SCA communication
        """
        print ('SCA Reset')
        self.rocWr(self.wr_add_ctr, 0x1, debug)
        self.waitBusy(debug)
        self.rd(debug)
        print ('SCA Init')
        self.rocWr(self.wr_add_ctr, 0x2, debug)
        self.waitBusy(debug)
        self.rd(debug)
        
        self.rocWr(self.wr_add_ctr, 0x0, debug)
    #--------------------------------------------------------------------------------    

    #--------------------------------------------------------------------------------    
    def reset(self, debug = None) :
        """
        RESET the SCA block inside the FPGA
        """
        self.rocWr(self.wr_add_ctr, 0x800000, debug)
        self.rocWr(self.wr_add_ctr, 0x0, debug)
    #--------------------------------------------------------------------------------    
    
    #--------------------------------------------------------------------------------    
    def displayAdd(self) :
        """
        Function to print the BASE ADD
        """
        print('-------------------------------------------------------')
        print('SCA ADD:')
        print('BASE ADD = %s ' %(hex(self.base_add)))
        print('-------------------------------------------------------')
    #--------------------------------------------------------------------------------    
        
    #--------------------------------------------------------------------------------    
    def gpioEn(self, debug = None) :
        """
        Enable the GPIO
        """

        trid = None
        
        # Enable GPIO
        # WR CONTROL REG B
        self.wr(0x00010002, 0xff000000, trid, debug)
        self.rd(debug)
        # RD CONTROL REG B
        self.wr(0x00020003, 0xff000000, trid, debug)
        self.rd(debug)
        
        # WR GPIO DIR
        #scaWr(ch, 0x02030020, 0xffffffff)
        self.wr(0x02030020, 0xffc003ff, trid, debug)
        self.rd(debug)
        # RD GPIO DIR
        self.wr(0x02040021, 0x0, trid, debug)
        self.rd(debug)    
    #--------------------------------------------------------------------------------

    #--------------------------------------------------------------------------------    
    def gpioWr(self, data, debug = None) :
        """
        Write 32 bit to the GPIO register
        """

        trid = None
        
        # WR REGISTER OUT DATA
        self.wr(0x02040010, data, trid, debug)
        # RD DATA
        self.wr(0x02050011, 0x0, trid, debug)
        self.rd(debug)
    #--------------------------------------------------------------------------------

    #--------------------------------------------------------------------------------    
    def gpioINTENABLE(self, data):
        """
        Function to access a GPIO register
        """
        # WR 
        self.wr(0x02010060, data)
        # RD
        self.wr(0x02020061, 0x0)
        self.rd()
    #--------------------------------------------------------------------------------

    #--------------------------------------------------------------------------------    
    def gpioINTSEL(self, data):
        """
        Function to access a GPIO register
        """        
        # WR 
        self.wr(0x02010030, data)
        # RD
        self.wr(0x02020031, 0x0)
        rd(ch)
    #--------------------------------------------------------------------------------

    #--------------------------------------------------------------------------------    
    def adcEn(self) :
        """
        Enable the ADC channel
        """
        # WR CONTROL REG D
        self.wr(0x00010006, 0xff000000)
        self.rd()
        # RD CONTROL REG B
        self.wr(0x00020007, 0xff000000)
        self.rd()
    #--------------------------------------------------------------------------------

    #--------------------------------------------------------------------------------
    def scaID(self):
        """
        Report the SCA ID
        """
        self.wr(0x140100D1, 0x00000001)
        self.rd()    
    #--------------------------------------------------------------------------------    

    #--------------------------------------------------------------------------------    
    def error(self, err_code):
        """
        Check the ERROR code returned by the SCA transaction
        """
        error_int = int(err_code,0)
        for i in range(0,8):
            error = error_int >> i
            error = error & 0x1
            
            if error == 1:
                if i == 0:
                    print('1 -> generic error flag')
                elif i == 1:
                    print('1 -> invalid channel request')
                elif i == 2:
                    print('1 -> invalid command request')
                elif i == 3:
                    print('1 -> invalid transactio number request')
                elif i == 4:
                    print('1 -> invalid length')
                elif i == 5:
                    print('1 -> channel not enabled')
                elif i == 7:
                    print('1 -> command in treatment')
                else:
                    print('0')
    #--------------------------------------------------------------------------------    

    #--------------------------------------------------------------------------------    
    def TpcCfgFile(self, file_name):
        """
        Update TPC config file name
        """
        if file_name == '':
            self.file_name = 'tpc_cmds'
        else :
            self.file_name = file_name
    #--------------------------------------------------------------------------------    

    #--------------------------------------------------------------------------------    
    def readbin(self, debug=None):
        """
        Return binary data from sca
        """
        #print("reading sca")
        err_cnt = 0
        err_int = 64
        while (err_int != 0):
            data = self.rocRd(self.rd_add_data, debug)
            cmd = self.rocRd(self.rd_add_cmd, debug)
            ctrl = self.rocRd(self.rd_add_ctr, debug)

            err_code = hex(cmd & 0xff)
            err_int = int(err_code, 0)

            tr_id = (cmd >> 16) & 0xff
            if (tr_id == 0):
                print('MAJOR ERROR ')
                #z.log('MAJOR ERROR, tr_id == 0', Errlvl.CRITICAL)
                return 1

            if err_int == 64:
                # CH BUSY
                time.sleep(1.0/1000.0)
            elif err_int != 0:
                # ERROR
                err_cnt += 1
                self.error(err_code)
                time.sleep(1.0/100.0)
                return 1
            else:
                #print('RD - DATA %10s CH %4s TR %4s ERR %4s CTRL %4s' % (hex(data), hex(cmd >> 24), hex((cmd >> 16) & 0xff), hex(cmd & 0xff), hex(ctrl)))
                return "{0:032b}".format(data)
#                return "RD - DATA {} CH {} TR {} ERR {} CTRL {}".format(hex(data), hex(cmd >> 24), hex((cmd >> 16) & 0xff), hex(cmd & 0xff), hex(ctrl))
#print('RD - DATA %10s CH %4s TR %4s ERR %4s CTRL %4s' % (hex(data), hex(cmd >> 24), hex((cmd >> 16) & 0xff), hex(cmd & 0xff), hex(ctrl)))
            return 0
    # --------------------------------------------------------------------------------
   
  #--------------------------------------------------------------------------------    
    def read_data(self, debug=None):
        """
        Return binary data from sca
        """
        #print("reading sca")
        err_cnt = 0
        err_int = 64
        while (err_int != 0):
            data = self.rocRd(self.rd_add_data, debug)
            cmd = self.rocRd(self.rd_add_cmd, debug)
            ctrl = self.rocRd(self.rd_add_ctr, debug)

            err_code = hex(cmd & 0xff)
            err_int = int(err_code, 0)

            tr_id = (cmd >> 16) & 0xff
            if (tr_id == 0):
                print('MAJOR ERROR ')
                assert False
                #z.log('MAJOR ERROR, tr_id == 0', Errlvl.CRITICAL)
                return -1

            if err_int == 64:
                # CH BUSY
                time.sleep(1.0/1000.0)
            elif err_int != 0:
                # ERROR
                err_cnt += 1
                self.error(err_code)
                time.sleep(1.0/100.0)
                return -1
            else:
                #print('RD - DATA %10s CH %4s TR %4s ERR %4s CTRL %4s' % (hex(data), hex(cmd >> 24), hex((cmd >> 16) & 0xff), hex(cmd & 0xff), hex(ctrl)))
                return data
#                return "RD - DATA {} CH {} TR {} ERR {} CTRL {}".format(hex(data), hex(cmd >> 24), hex((cmd >> 16) & 0xff), hex(cmd & 0xff), hex(ctrl))
#print('RD - DATA %10s CH %4s TR %4s ERR %4s CTRL %4s' % (hex(data), hex(cmd >> 24), hex((cmd >> 16) & 0xff), hex(cmd & 0xff), hex(ctrl)))
            return 0
    # --------------------------------------------------------------------------------
   
