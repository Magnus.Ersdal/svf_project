
class Utils:
    def invertByte(self, data):
        l = data
        n = 2
        ll = [l[i:i+n] for i in range(0, len(l), n)]

        data = '0x' + ll[4] + ll[3] + ll[2] + ll[1]
        return data

    def checkRet(self, data, ret):
        if data != ret:
            print('ERROR : DATA WR %s, DATA RD %s' % (hex(data), hex(ret)))
