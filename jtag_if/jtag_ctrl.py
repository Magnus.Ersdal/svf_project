# -*- coding: utf-8 -*-
"""
Created on Fri Apr  6 11:15:35 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
"""

#from hwdriver.SCA import Sca
from SCARegs import SCARegs
from zmqsender_config import Errlvl
from settings import FIFOLEN, FREQUENCY, highTMS_bin_zip, TMSregs, TDOregs
import math
#z = settings.z
#sca = settings.sca
#jtg_seq

class Control():
    def __init__(self, sca):
        self.sca = sca
        self.power()
        scregscfg = 0x4000 | 0b10000000000
        self.sca.wr(SCARegs['JTAG_W_CTRL'], 0x0000)#400 600 # 0x4000 invert clock.
        
    def getsca(self):
        print("returns sca object",self.sca)
        return self.sca
    
    def insertseq(self,sequencer):
        self.jtg_seq = sequencer
        
    def setfmax(self):
        #        print("setmax freq")
        self.sca.wr(SCARegs['JTAG_W_FREQ'], 0x0, None)

    def setf(self, hz):
        self.frequency = hz
        div = (2*10**7 / hz) - 1
        div = int(math.floor(div))
#        self.sca.wr(SCARegs['JTAG_W_FREQ'], div, None)
        self.sca.wr(SCARegs['JTAG_W_FREQ'],div)
#        print("put frequency to", hz, "Hz with div:", div)

    def power(self):
        # turn on jtag interface
        #self.sca.wr(SCARegs['CTRL_W_CRD'], 0xFF000000)
        print("turn on JTAG channel")
        self.safewrite(SCARegs['CTRL_W_CRD'],
                       SCARegs['CTRL_R_CRD'], 0xFF000000, (0b00001000 << 24))
        

#    def set_fifo_len(self, length):
#        """WRITE 0 for 128 length, 1-127 for 1-127 length"""
#        if self.fifo_len == 128:
#            newlength = 0
#        else:
#            newlength = length
#        if (length % 32) != 0:
#        self.safewrite(SCARegs['JTAG_W_CTRL'],
#                       SCARegs['JTAG_R_CTRL'], newlength, 0x3f)
#        self.prev_fifo_len = self.fifo_len
        
    def safewrite(self, wregister, rregister, data, mask):
        """Safe write config data with mask."""
        print("safewrite")
        print(hex(wregister), hex(rregister), hex(data), hex(mask))
        self.sca.wr(rregister, 0, None)
        i = self.sca.rd()
        if i == None:
            i = 0
        #print ("i is ", i)
        notmask = (~mask & 0xFFFFFFFF)  # because sign
        #print("performing", bin(data), "&", bin(mask), "|", bin(i), "&", bin(notmask))
        value = (data & mask) | (i & notmask)
        self.sca.wr(wregister, value, None)
        
    def go(self):
        #        print("GO!")
        # check this with version number and sca manual
        self.sca.wr(SCARegs['JTAG_GO'], 0)
        
    def wait(self, duration, tckorsec = "TCK", waitstate = 'IDLE'):
        """Wait for n clock cycles"""
        #print("waits called, duration:{}, state{}".format(duration,waitstate))
        n_clk_waits = 0
        if tckorsec == 'SEC':
            n_clk_waits = int(duration * FREQUENCY)
            pass
        if tckorsec == 'TCK':
            n_clk_waits = int(duration)
        n_32s = int(n_clk_waits/32)
        remwaits = n_clk_waits%32
        if n_32s > 10000:
            bign = 10000
            
            extra = self.jtg_seq.flush_tqueue() # flush queue returns the extra bits it had to append
            
            n_128s = int((n_clk_waits-extra)/128)
            remwaits = (n_clk_waits-extra)%128 # fix the extra bits
                  
            print("entered loooong wait state")
            part_128s = int(n_128s/bign)
            #zerotuples = [(0,32)] * part_32s
            remwaits += (n_128s%bign)*128
            
            if waitstate == 'RESET':
                #tmstuples = [(0xFFFFFFFF, 32)] * part_32s
                tmslist=[0xFFFFFFFF] * 4
                rem_tmstuple =  (int("1"*remwaits, 2), remwaits)
                rem_tdituple = (0, remwaits)
            else:
                tmslist=[0] * 4
                rem_tmstuple =  (0, remwaits)
                rem_tdituple = (0, remwaits)
            # first step is to fill tms with Zero.
            _ = [self.sca.wr(reg, data) for reg,data in zip(TMSregs, tmslist)] # write to all four TMS registers 
            
            #iterator = zip (TDOregs, [0]*4)
            trid = 33
            for i in range(n_128s):
#                _ = [self.sca.wr(reg, data) for reg,data in iterator]
                # manual loop unroll here
                
                self.sca.wr_fast(TDOregs[0],0,trid)
                self.sca.wr_fast(TDOregs[1],0,trid)
                self.sca.wr_fast(TDOregs[2],0,trid)
                self.sca.wr_fast(TDOregs[3],0,trid)
                self.sca.wr_fast(SCARegs['JTAG_GO'], 0,trid)
#            for i in range(part_32s):
#                self.jtg_seq.tqueue(tmstuples, zerotuples)
            
            if remwaits:
                self.jtg_seq.tqueue([rem_tmstuple],[rem_tdituple])
            ## what I really want to to is to 1 : empty the jtg_seq queue
            ## then write directly, saving 50%. 
            print("done with long wait")
            
        else:
            if waitstate == 'RESET':
                print("waitstate reset!")
                tmstuples = [(0xFFFFFFFF, 32)] * n_32s
                tdituples = [(0,32)] * n_32s
                if remwaits:
                    tmstuples.append( (int("1"*remwaits, 2), remwaits) )
                    tdituples.append((0, remwaits))
                self.jtg_seq.tqueue(tmstuples, tdituples)
            else:
                tuples = [(0,32)] * n_32s
                if remwaits:
                    tuples.append((0, remwaits))
                self.jtg_seq.tqueue(tuples, tuples)
            
    