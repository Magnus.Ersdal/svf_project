# -*- coding: utf-8 -*-
"""
Created on Wed Mar 21 14:24:55 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
Specific JTAG interface for the GBT-SCA chip connected to the CRU via
fibre-optic GBT link
"""
from hwdriver.SCA import Sca
from jtag_if import jtag_tap as jtap
from SCARegs import SCARegs
from zmqsender_config import Errlvl
import settings
z = settings.z

import time
from math import ceil

four_32b_high = [0xff_ff_ff_ff] * 4
TMSregs =[SCARegs['JTAG_W_TMS0'], SCARegs['JTAG_W_TMS1'],
                    SCARegs['JTAG_W_TMS2'], SCARegs['JTAG_W_TMS3']]
TDOregs =[SCARegs['JTAG_W_TDO0'], SCARegs['JTAG_W_TDO1'],
                    SCARegs['JTAG_W_TDO2'], SCARegs['JTAG_W_TDO3']]
highTMS_zip = zip(TMSregs, four_32b_high)


class interface():  # can have multiple inheritance to hide methods.
    def setfmax(self):
        #        print("setmax freq")
        self.sca.wr(SCARegs['JTAG_W_FREQ'], 0x0, None, self.scadebug)

    def setf(self, hz: float):
        self.frequency = hz
        div = (2*10**7 / hz) - 1
        div = int(div).__floor__()
        self.sca.wr(SCARegs['JTAG_W_FREQ'], div, None, self.scadebug)
#        print("put frequency to", hz, "Hz with div:", div)

    def power(self):
        # turn on jtag interface
        self.safewrite(SCARegs['CTRL_W_CRD'],
                       SCARegs['CTRL_R_CRD'], 0xFF000000, 0x07000000)
#        print("turn on JTAG channel")

    def __init__(self):
        id_card = 1
        gbt_ch = 0
        board = "CRU"
        self.scadebug = None
        self.sca = Sca(id_card, 2, gbt_ch, board, self.scadebug)
        self.power()
        self.frequency = 20e6
        self.tap = jtap.tap()
        self.tms_queue = []
        self.tdi_queue = []
        self.fifo_len = 0  # length of JTAG data.
        self.prev_fifo_len = 0
        self.tms_cycle = 0  # tms needs bus cycling
        self.tdi_cycle = 0  # tdi needs bus cycling
        print("warning deprecated module component \"jtag_interface.py\" has started")
        z.log("warning deprecated module component \"jtag_interface.py\" has started",Errlvl.MEDIUM)

    def _tms_write(self, vect: str):
        self._write_TMS_TDI(vect, 'TMS')

# unused
#    def tdi_write(self, data: int):
#        self._tdi_write(self.int2bin(data))

    def _tdi_write(self, vect: str):
        self._write_TMS_TDI(vect, 'TDI')

    def _set_fifo_len(self):
        """WRITE 0 for 128 length, 1-127 for 1-127 length"""
        if self.fifo_len == 128:
            newlength = 0
        else:
            newlength = self.fifo_len
        self.safewrite(SCARegs['JTAG_W_CTRL'],
                       SCARegs['JTAG_R_CTRL'], newlength, 0x3f)
        self.prev_fifo_len = self.fifo_len

    def _go(self):
        #        print("GO!")
        # check this with version number and sca manual
        self.sca.wr(SCARegs['JTAG_GO'], 0)
        self.tms_cycle = 0
        self.tdi_cycle = 0

    # function that writes either tms OR tdi
    def _write_TMS_TDI(self, vect: str, bustype: str = 'TDI', both: bool = False):
        """SCA JTAG TMS and TDI signal writer"""
        if bustype == 'TDI':
            regs = TDOregs
            self.tdi_cycle = 1
        else:
            regs = TMSregs
            self.tms_cycle = 1
        if 0 < len(vect) <= 128:  # this is the case for writing 1 - 128 bits into TMS/TDI
            self.fifo_len = len(vect)
            print("{}: vector is {}, length is {}".format(bustype,vect,len(vect)))
            if self.fifo_len != self.prev_fifo_len:
                print("set fifo length")
                self._set_fifo_len()
#            print("got vector:", hex(int(vect, 2)))
            strs = self.split(vect, 32)
            # turn binary string into int, first change the strings order
            # PROBLEM ! "0000" will get turned into 0 !!!
            # but there is nothing we can do, because it is handled as ints on the driver level
            ints = [int(self.fliporder(strs[i]), 2) for i in range(len(strs))]
            regs_o = regs[0:len(ints)]
            # need setlen and previous len
            # fill registers.
            for reg, data in zip(regs_o, ints):
                self.sca.wr(reg, data)
#            print("wrote sca reg of data")
            # cycle bus
            if both:
                pass
            else:
                self._bus_cycle()

        elif len(vect) > 128:
            self._write_TMS_TDI_long(vect, bustype)
        else:
            print("ERROR, tms_write_vector zero length")
        if bustype == 'TMS' and len(self.tms_queue) > 0:
            #            print("tms queue contains:", self.tms_queue[0])
            self._tms_write(self.tms_queue.pop(0))
        if bustype == 'TDI' and len(self.tdi_queue) > 0:
            #            print("tdi queue contains:", self.tdi_queue[0])
            self._tdi_write(self.tdi_queue.pop(0))

    def _write_TMS_TDI_long(self, vect: str, bustype: str = 'TDI'):
        long_strs = self.split(vect, 128)
        if bustype == 'TDI':
            [self.tms_queue.append(x) for x in long_strs[1:]]
        else:
            [self.tdi_queue.append(x) for x in long_strs[1:]]
        self._write_TMS_TDI(long_strs[0], bustype)

    def _write_both_TMS_TDI(self, TMSvect, TDIvect):
        assert len(TMSvect) == len(TDIvect)
#        print("length of vectors are:", len(TMSvect))
        if len(TDIvect) > 128:#this splits and alternates between the writes.
            TMS_s = self.split(TMSvect, 128)
            TDI_s = self.split(TDIvect, 128)
            for TMS, TDI in zip(TMS_s, TDI_s):
                self._write_TMS_TDI(TMS, 'TMS', True)
                self._write_TMS_TDI(TDI, 'TDI', True)
                self._bus_cycle()
        elif len(TDIvect) < 128:
            self._write_TMS_TDI(TMSvect, 'TMS', True)
            self._write_TMS_TDI(TDIvect, 'TDI', True)
            self._bus_cycle()
            

    def read_TDO(self):
        pass

    def _bus_cycle(self):
        """uses self.tms_cycle and self.tdi_cycle as interlocks to start the bus"""
        if self.tms_cycle and self.tdi_cycle:  # we will cycle the bus
            self._go()
        elif self.tms_cycle:  # we need tdi bits
            self._tdi_write("0" * self.fifo_len)
        elif self.tdi_cycle:  # we need tms bits
            self._tms_write("0" * self.fifo_len)

    def scan_dr_ir(self, header, data, trailer, endstate, mode='DR'):
        assert (mode == 'DR' or mode == 'IR')
   #     print("jtag received scan command, data:",header,data,trailer)
#        print("moving from ",self.tap.current_state, "to SHIFT")
        """
        Make TMS sequence to set TAP to proper state eg. IDLE -> SHIFT_DR 
        Append header and trailer to the DATA
        Append trailer on TMS that allows data to be shifted into device. (function of whole data length plus length of TMS init)
        Append trailer on TMS that moves it onto the Specified END state.!
        """
        if mode == 'DR':
            shiftstate = 'DRSHIFT'
        elif mode == 'IR':
            shiftstate = 'IRSHIFT'
        # the following generates proper TMS and TDI signals.
        # The standard only defines SIR or SDR, so this is where we fix
        # the interface between SIR/SDR and actually making a TMS signal to
        # get the TAP state machines into their respective states.
        tms_a = self.tap.state_change(shiftstate)
        tms_b = "0" * (len(header) + len(data) + len(trailer))
        tms_c = self.tap.state_change(endstate)

        tdi_a = "0" * len(tms_a)
        tdi_b = header + data + trailer
        tdi_c = "0" * len(tms_c)

        TMS_binary = tms_a + tms_b + tms_c
        TDI_binary = tdi_a + tdi_b + tdi_c

#        print("scan",mode," writing")

        self._write_both_TMS_TDI(TMS_binary, TDI_binary)

    def scan_dr(self, HDR, SDR, TDR, enddr):
        #        print("jtag received DR scan command")
        self.scan_dr_ir(HDR, SDR, TDR, enddr, 'DR')

    def scan_ir(self, HIR, SIR, TIR, endir):
        #        print("jtag received IR scan command")
        self.scan_dr_ir(HIR, SIR, TIR, endir, 'IR')

    def test_tms_write(self, vect: str):
        #        print("WARNING, only for test access")
        self._tms_write(vect)

    def state_change(self, state):
        """make TMS sequence for changing TAP state, then enforce the TAP change
        Generic for ALL JTAG interfaces"""
        tms_temp = self.tap.state_change(state)
        self._tms_write(tms_temp)  # 22.03.2018 todo 23.03.2018

    def wait(self, duration, tckorsec: str = "TCK", waitstate = 'IDLE'):
        """Wait for n clock cycles"""
        f_clk = self.frequency
        n_clk_waits = 0
        if tckorsec == 'SEC':
            n_clk_waits = duration * f_clk
            pass
#            print("seconds,kek")
        if tckorsec == 'TCK':
            n_clk_waits = duration
#            nperiods = duration
#            duration = nperiods * 1/self.frequency
#            time.sleep(duration)
#            print("clocksycles,kek")
        if int(n_clk_waits % 128) == 0:
            self.fifo_len = 128
        print("length is  {}".format(self.fifo_len))
        if waitstate == 'RESET':
#            bs = '1' * int(n_clk_waits)
            self.wait_tms_high(n_clk_waits)
        else:
            self.wait_all_low(n_clk_waits)
#            bs = '0' * int(n_clk_waits)
#        z.log("sleep for {} clocks".format(n_clk_waits), Errlvl.DEBUG1)    
        # technically, i just need to say GO for x times 
#        self._write_both_TMS_TDI(bs,bs)

    def wait_all_low(self,n_clk_waits):
        n_runs = int(n_clk_waits / self.fifo_len)
        for i in range(n_runs):
            self._go()
  
    def wait_tms_high(self,n_clk_waits):
        n_runs = int(n_clk_waits / self.fifo_len)
        for i in range(n_runs): #highTMS_zip
            for reg, data in highTMS_zip:
                self.sca.wr(reg, data)
            self._go()

    def safewrite(self, wregister: int, rregister: int, data: int, mask: int):
        """Safe write config data with mask."""
        self.sca.wr(rregister, 0, None, self.scadebug)
        i = self.sca.rd(self.scadebug)
        #print ("i is ", i)
        notmask = (~mask & 0xFFFFFFFF)  # because sign
        #print("performing", bin(data), "&", bin(mask), "|", bin(i), "&", bin(notmask))
        value = (data & mask) | (i & notmask)
        self.sca.wr(wregister, value, None, self.scadebug)

    def split(self, vect: str, chunksize: int):
        """Split string into chunksize pieces, return list"""
        nwords = ceil(len(vect)/chunksize)
        # https://stackoverflow.com/users/417685/alexander
        return [vect[i:i+chunksize] for i in range(0, nwords*chunksize, chunksize)]

    def fliporder(self, vect: str):
        """Flip binary string back to front, return string"""
        return vect[::-1]

    def hexstring2bin(self, hexstring):
        """Return binary string from hex string with no prefix"""
        return "{0:b}".format(int(hexstring, 16))

# unused
#    def int2bin(self, integer):
#        return self.fliporder("{0:b}".format(int(integer)))

