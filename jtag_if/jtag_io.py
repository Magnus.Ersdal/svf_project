# -*- coding: utf-8 -*-
"""
Created on Fri Apr  6 12:00:31 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
"""


from SCARegs import SCARegs
from zmqsender_config import Errlvl
import settings
#z = settings.z
TMSregs = settings.TMSregs
TDOregs = settings.TDOregs
TDIregs = settings.TDIregs
tms_tdi_list = TMSregs + TDOregs

zeros = "0" * 32

def emap(func, *iterables):
    return list(map(func, *iterables))

def write128(tms_bits, tdi_bits, sca):
    #print("writing tms:{} tdi:{}".format(tms_bits, tdi_bits))
    tms_list = [tms_bits[0:32], tms_bits[32:64], tms_bits[64:96], tms_bits[96:128]]
    tdi_list = [tdi_bits[0:32], tdi_bits[32:64], tdi_bits[64:96], tdi_bits[96:128]]
    for tms_reg,tms_data in zip(settings.TMSregs, tms_list):
        sca.wr(cmd=tms_reg, data =int(tms_data[::-1],2))#int(tms_data,2))# # NB invert bits here
    for tdo_reg,tdo_data in zip(settings.TDOregs, tdi_list):
        sca.wr(cmd=tdo_reg, data =int(tdo_data[::-1],2))
    #print("go JTAG")
    sca.wr(SCARegs['JTAG_GO'], 0)


def read128(sca):
    data=[]
    for reg in settings.TDIregs: # naming of SCA regs is incoherent with the svf standard
        sca.wr(cmd = reg, data = 0)
        data.append(sca.readbin())
    s=""
    datastring = s.join(data)
    return datastring

class trid():
    def __init__(self):
        self.trid = 1
    def get(self):
        tmp = self.trid
        self.trid +=1
        if self.trid == 0xff:
            self.trid = 1
        return tmp

T = trid()

def read_4_ints(sca):
    data=[]
    tmp = T.get()
    for reg in settings.TDIregs: # naming of SCA regs is incoherent with the svf standard
        sca.wr(cmd = reg, data = 0, trid=tmp) # provide TRID
        data.append(sca.read_data())
    return data

def write_4_ints(tms_list, tdi_list, sca):
    tmp = T.get()
    #print("writing to HW:")
    #print("tms:", [hex(x) for x in tms_list])
    #print("tdi:", [hex(x) for x in tdi_list])
    for reg, data in zip(TMSregs,tms_list):
        sca.wr(reg,data,tmp)
    for reg, data in zip(TDOregs,tdi_list):
        sca.wr(reg,data,tmp)
#    print("writing list {}".format(tms_list))
#    emap(sca.wr, tms_tdi_list, tms_list + tdi_list) # did not play nice with C interface
#    for tmsreg, tdireg, tmsdata, tdidata in zip(TMSregs,TDOregs,tms_list,tdi_list):
#        sca.wr(tmsreg,tmsdata)
#        sca.wr(tdireg,tdidata)
    sca.wr(SCARegs['JTAG_GO'], 0,tmp)
