# -*- coding: utf-8 -*-
"""
Created on Fri Apr  6 11:28:43 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
"""

 
from jtag_if.jtag_io import write128, read128, write_4_ints, read_4_ints
from components.tupleconv import tpli_2_32ili, listof32s_append_tuplelist
 
FIFOLEN = 128

class Seq():
    def __init__(self,sca):
        self.tms_bits = ""
        self.tdi_bits = ""
        self.tdo_bits = []  # store of TDO bits.
        self.sca = sca
        self.tms_tuples = []
        self.tdi_tuples = []
        self.tms_32ints = []
        self.tdi_32ints = []
        self.qlength = 0
        self.output = []
        
    def check_to_run(self, length):
        if length >= FIFOLEN:
            self.call_run(length)

    def call_run(self,length):
        # have checked that they are different HERE
        write128(self.tms_bits[:FIFOLEN], self.tdi_bits[:FIFOLEN], self.sca) #write
        self.tdo_bits.append(read128(self.sca)) # read
        self.tms_bits = self.tms_bits[FIFOLEN:] 
        self.tdi_bits = self.tdi_bits[FIFOLEN:]
        self.check_to_run(length - FIFOLEN)

    def check_t_run(self):
        if (len(self.tms_32ints) >= 4) or (self.qlength >= FIFOLEN):
            try:
                assert sum([item[1] for item in self.tms_tuples]) == sum([item[1] for item in self.tdi_tuples])
            except AssertionError:
                print("error! from sequencer, line 44")
                print(self.tms_tuples)
                print("and")
                print(self.tdi_tuples)
                print("does not match in lengths") 
            self.tms_tuples = listof32s_append_tuplelist(self.tms_32ints, self.tms_tuples)

            self.tdi_tuples = listof32s_append_tuplelist(self.tdi_32ints, self.tdi_tuples)
#            print("tdi after:")
            self.qlength = sum([i[1] for i in self.tms_tuples]) # needs update
            # all our lists are now updated.
            l32len = len(self.tdi_32ints)
            try:
                assert len(self.tdi_32ints) == len(self.tms_32ints)
            except AssertionError:
                print("error! from sequencer line 69")
                print(self.tdi_32ints, "not equal length of", self.tms_32ints)
                assert len(self.tdi_32ints) == len(self.tms_32ints)
            l128 = int(l32len/4)
#            lrem = l128 * 4 - l32len
            # right from left: a[:-10:-1] left from right : a[-9:]
            ### the loop starts here:
            rangesel = 4
            #print([hex(x) for x in self.tdi_32ints[:rangesel]])
            for i in range(l128):
                write_4_ints(self.tms_32ints[:rangesel], self.tdi_32ints[:rangesel],self.sca)
                self.tms_32ints[:rangesel] = []
                self.tdi_32ints[:rangesel] = []
#                self.qlength -= 128
                #self.output.append(read_4_ints(self.sca))
    
    def get_tdo_bits(self):
        m = self.tdo_bits
        self.tdo_bits = []
        return "".join(m)
    
    def get_tdo_bits_list(self):
        return self.tdo_bits
    
    def queue(self, tms_bits, tdi_bits):
        """fill a queue with bits for the JTAG"""
        self.tms_bits += tms_bits # testing with reversal.
        self.tdi_bits += tdi_bits
        self.check_to_run(len(self.tms_bits))

    def tqueue(self, tms_tuples, tdi_tuples):
        """fill a tuplequeue"""
        #print("Queing", tms_tuples, "tdi:", tdi_tuples)
        #self.tms_tuples.append(tms_tuples)
        self.tms_tuples += tms_tuples
        #self.tdi_tuples.append(tdi_tuples)
        self.tdi_tuples += tdi_tuples
        deltal =sum([item[1] for item in tms_tuples]) - sum([item[1] for item in tdi_tuples])
        #print("tq tdi:", self.tdi_tuples)
        #print("tq tms:", self.tms_tuples)
        try:
            assert deltal == 0
        except ValueError:
            print("error! deltal =", deltal)
        self.qlength += sum([i[1] for i in tms_tuples])
        self.check_t_run()

    
    def flush_tqueue(self, ntuple = None):
        while self.qlength > 128:
            self.check_t_run()            
        if self.qlength > 0:
            # we have some data. which is less than 128 long.
            nlen = abs(128 - self.qlength)
            if ntuple:
                pass
            else:
                ntuple = (0,nlen)
            # self.qlength should make out to be 128 now
            #print("queing tuples")
            #print("ztuple:", [ntuple])
            self.tqueue([ntuple],[ntuple]) # and this pipes out
            return nlen
        else:
            return 0
        
    def end_clean(self):
        """write out any remaining bits"""
        self.flush_tqueue()
#        self.check_to_run(len(self.tms_bits))
#        length = len(self.tms_bits) # guaranteed to be between 0 and FIFOLEN-1
#        if length > 0: # fill with zeros
#            self.tms_bits += "0" * (FIFOLEN - length)
#            self.tdi_bits += "0" * (FIFOLEN - length)
#            self.check_to_run(len(self.tms_bits))
#        else:
#            pass
        print("sequencer complete")
        
def andlist(list1, list2): 
    return list(map(lambda x,y: x & y, list1, list2))

#def tuple_list_to_32int_list(list_32i, tuplelist):
#    list_32i, tuplelist = listof32s_append_tuplelist(list_32i, tuplelist)
#    tmp_32list, tmp_remi, temp_reml = tpli_2_32ili(tuplelist) #sort
#    tuplelist = [] # clear
#    list_32i += tmp_32list # append items
#    tuplelist.append((tmp_remi, temp_reml)) # append item

#def tpli_2_32ili(tuples):
#    '''make a list of tuples of (int, length) into a list of 32 bit integers [1,2,3]'''
##    binarylist = "".join([np.binary_repr(a, b) for a, b in inp]) # bin(a)[2:].rjust(b, '0')
#    binarylist = "".join([bin(a)[2:].rjust(b, '0') for a, b in tuples])
#    totallength = len(binarylist)
#    tot32 = int(totallength/32)
#    i32list = [int(binarylist[i:i+32],2) for i in range(0, tot32*32, 32) ]
#    remlen = totallength - tot32*32
#    remint = int(binarylist[-remlen:],2)
#    return i32list, remint, remlen

#def memoize2(f):
#    memo = {}
#    def helper(x,x1):
#        if (x,x1) not in memo:            
#            memo[(x,x1)] = f(x,x1)
#        return memo[(x,x1)]
#    return helper
#
#@memoize2
#def reverse_int(number, length):
#    return b2i(revb(i2b(number, length)))
#
#
#def i2b(number, length):
#    return "{0:0{length}b}".format(number, length=length)
#
#
#def revb(binnum):
#    return binnum[::-1]
#
#
#def b2i(binnum):
#    return int(binnum, 2)
