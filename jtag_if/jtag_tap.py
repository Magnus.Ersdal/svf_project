# -*- coding: utf-8 -*-
"""
Created on Thu Mar 22 17:50:16 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
"""
from svf_dictionary import tap_graph
from simplegraph import find_shortest_path


class tap():
    def __init__(self):
        self.current_state = 'RESET'
        self.state_path = []
        self.memo = {}
        pass
    def __del__(self):
        print("exiting tap")
        print("memoized values:")
        print(self.memo)
        print("----------")

    def __make_tms_seq(self):
        """make binary TMS signal from state transition list"""
        tms_string = ""
        tapstates = self.state_path
        # for multiple paths
        if len(tapstates) > 1:
            for idx, state in enumerate(tapstates):
                if idx < (len(tapstates) - 1):
                    nextstate = tapstates[idx+1]
                    tms_string = tms_string + \
                        str(tap_graph[state].index(nextstate))

        if len(tapstates) == 1:
            state = tapstates[0]
            tms_string = tms_string + str(tap_graph[state].index(state))
        return tms_string

    def state_change(self, state):
        '''memoized state-change function'''
        x = (self.current_state, state)
        if x in self.memo:
            tms_string = self.memo[x]
        else:
            # get the required tap state path
            self.state_path = find_shortest_path(
                    tap_graph, self.current_state, state)
            #print("found shortest path from",self.current_state, "to", state, ":", self.state_path)
            tms_string = self.__make_tms_seq()
            self.memo[x] = tms_string
        
        self.current_state = state
#        tdi_string = "0" * len(tms_string)
        #print(state, "TMS:",tms_string)
        return tms_string
