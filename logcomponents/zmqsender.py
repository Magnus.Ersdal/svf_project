# -*- coding: utf-8 -*-
"""
Created on Tue Apr  3 13:57:14 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
sending to server_with_buffer.py
operation completed in 0 minutes 14.143425 seconds with no windows antivirus and tcp with the string int(i)
16 seconds with a longer string.  
1e6/14 = 71 000
"""

import zmq
from zmqsender_config import Errlvl
from zmqsender_config import severity_report, ecompare

class Sender():
    """Fast Log sender class"""
    def __init__(self):
        self.i = 0
        self.ctx = zmq.Context()
        self.sck = self.ctx.socket(zmq.PUSH)   #context.socket(zmq.PULL)
        self.sck.connect("tcp://localhost:5557")
    def log(self, logstring, errlvl = Errlvl.LOW):
        if ecompare(severity_report, errlvl):
            self.sck.send_string(logstring)
    def close(self):
        status = 0
        return status
    def __enter__(self):
        return self
    def __exit__(self, type, value, traceback):
        s = self.close()
""" usage:         
with Sender() as snd:
    try:
        print("starting sender")
        time.sleep(1)
        snd.log("start of log at {}".format(time.asctime()))
        sw1 = Stopwatch()
        for i in range(int(1e6)):
            snd.log("a longer logstring" + str(i))
    except KeyboardInterrupt:
        print("----- C-C detected! -----")
    finally:
        snd.log("end of log at {}".format(time.asctime()))
        sw1.stop()
"""
