# -*- coding: utf-8 -*-
"""
Created on Mon Apr  9 10:42:00 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
RAW sca reader
"""

from hwdriver.SCA import Sca
from settings import z, TDIregs

id_card = 1
gbt_ch = 0
board = "CRU"
sca = Sca(id_card, 2, gbt_ch, board, logger = z)

print("input \"q\" to quit")
try:
    while(True):
        inp = input()
        if inp == 'q':
            break    
        else:
            for reg in TDIregs:
                sca.wr(reg,0)
                print(sca.rdstr())
                print(sca.readbin())
except KeyboardInterrupt:
    print("C-C caught")
    