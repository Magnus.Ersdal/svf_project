svf project
requires from pip
* fire
* pyzmq

The purpose of this code is to program the ITS RU Microsemi ProAsic3 FPGA.
It communicates from a FLP/(a server) with the CRU card and libReadoutCard driver from O²

The IO is using the SCA.py read and write methods. This file is slightly modified from the original ITS version.

The driver communicates with the CRU FW. In the CRU FW there is a hardware HDLC wrapper that sends the data down the GBT links EC field.

#Procedure to start programming the PA3 chip:#

requirements:
* FLP with CRU/Altera dev-board programmed. 
* everything CRU set-up. 

* No other programmers plugged in to the PA3 board

* GBT chips set up, either with dongle or fusing (don't fuse until we know final config)

* Jumpers should be in correct positions: 
(reference: with the backplane connector to the top/north/up)
J33 always to the right
J34 to the right for ONLY PA3 in the chain
J34 to the left for production configuration
J39 *must* be disconnected

Then it should be as simple as to change the configuration of the script to target your specific board and .svf file, then launch the tool. 

