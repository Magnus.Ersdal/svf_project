"""
author = magnus.ersdal@uib.no
"""

from __future__ import print_function

print("start!")
import svf_intepreter as interpreter
from stopwatch import Stopwatch
from svf_separate_commands import read_svf
from hwdriver.SCA import Sca
import sys
from components.get_cfg import get_configuration_variables

#     opt = ['id_card','board','gbt_ch','svf_filename']
def configure_PA3(cfgfile_name):
    """ Worker that programs a PA3 """
    print("got cfgfile_name {}".format(cfgfile_name))
    cfg = get_configuration_variables(cfgfile_name)

    instruction_list = read_svf(cfg['svf_filename'])

    sw1 = Stopwatch()

    sca = Sca(cfg["id_card"], 2, cfg["gbt_ch"], cfg["board"])

    try:
        svf = interpreter.svfinterpreter(sca, cfg)
        print("")
        for vect in instruction_list:
            svf.caller(vect)
        remli = svf.end()
    except KeyboardInterrupt:
        print("----- C-C detected! -----")
    finally:
        sw1.stop()
