# -*- coding: utf-8 -*-
"""
Created on Tue Apr  3 16:32:58 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
global objects, required for many blocks of the software.
"""
#import hwdriver.SCA
from SCARegs import SCARegs
from logcomponents.zmqsender import Sender
#import jtag_if.jtag_sequencer as jtag_seq
import jtag_if.jtag_tap as jtag_tap

#def init():
#    global z
z = Sender()

#id_card = 1
#gbt_ch = 0
#board = "CRU"
#
#sca = hwdriver.SCA.Sca(id_card,2,gbt_ch,board, z)

four_32b_high = [0xffffffff] * 4
four_32b_high_str = ["1" * 32] * 4

TMSregs = [SCARegs['JTAG_W_TMS0'], SCARegs['JTAG_W_TMS1'],
                    SCARegs['JTAG_W_TMS2'], SCARegs['JTAG_W_TMS3']]

TDOregs = [SCARegs['JTAG_W_TDO0'], SCARegs['JTAG_W_TDO1'],
                    SCARegs['JTAG_W_TDO2'], SCARegs['JTAG_W_TDO3']]

TDIregs = [SCARegs['JTAG_R_TDI0'], SCARegs['JTAG_R_TDI1'],
                    SCARegs['JTAG_R_TDI2'], SCARegs['JTAG_R_TDI3']]

highTMS_zip = zip(TMSregs, four_32b_high)

highTMS_bin_zip = zip(TMSregs, four_32b_high_str)


FIFOLEN = 128

FREQUENCY = 20e6

#jtg_seq = jtag_seq.Seq()

jtg_tp = jtag_tap.tap()
