# -*- coding: utf-8 -*-
"""
Created on Thu Mar 22 10:02:15 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
ported from python2 to python3, 
orginial source: https://www.python.org/doc/essays/graphs
"""


def find_path(graph, start, end, path=[]):
    path = path + [start]
    if start == end:
        return path
    if not start in graph:
        return None
    for node in graph[start]:
        if node not in path:
            newpath = find_path(graph, node, end, path)
            if newpath:
                return newpath
    return None


def find_all_paths(graph, start, end, path=[]):
    path = path + [start]
    if start == end:
        return [path]
    if not start in graph.keys():
        #    print("error, start node not in graph")
        return []
    paths = []
    for node in graph[start]:
        if node not in path:
            newpaths = find_all_paths(graph, node, end, path)
            for newpath in newpaths:
                paths.append(newpath)
    return paths

"""
def find_shortest_path(graph, start, end, path=[], level=0):
    #print("start:", start, "end:",end)
    path = path + [start]
#    print("PATH :",path, "start",start,"end",end)
    findreturnpath = 0
    if level == 0 and start == end and end not in graph[start]:
        findreturnpath = 1
    if start == end and findreturnpath == 0:
        return path
    if start == end and findreturnpath == 1 and level == 0:
        # from the next node, which is the fastest way to the end?
        alt0, alt1 = graph[start]  # get the two alternatives
        a = find_shortest_path(graph, alt0, end, [], level=0)
        b = find_shortest_path(graph, alt1, end, [], level=0)
        mini = min(len(a), len(b))
        if len(a) == len(b):
            # lengths are equal.
            if 'IDLE' in a:
                shortest = a
            if 'IDLE' in b:
                shortest = b
            else:
                shortest = a

        else:
            if mini == len(a):
                shortest = a
            else:
                shortest = b
        path = [start] + shortest
        return path
"""
def find_shortest_path(graph, start, end, path=[]):
    """this is called first, fsp_post is called second"""
    #print("start:", start, "end:",end)
    path = path + [start]
#    print("PATH :",path, "start",start,"end",end)
    if start == end and end not in graph[start]:
        # from the next node, which is the fastest way to the end?
        if len(graph[start]) == 2:
            alt0, alt1 = graph[start]  # get the two alternatives
            first = find_shortest_path(graph, alt0, end, [])
            second = find_shortest_path(graph, alt1, end, [])
            shortest = fsp_select_shortest(first, second)
        else:
            k = graph[start]
            shortest = find_shortest_path(graph, k[0] , end, [])
        path = [start] + shortest
        return path
    elif start == end:
        return path
    if not start in graph.keys():
       #     print("error, start node not in graph")
        return None
    shortest = None
    for node in graph[start]:
        if node not in path:
            newpath = fsp_post(graph, node, end, path)
            if newpath:
                if not shortest or len(newpath) < len(shortest):
                    shortest = newpath
    return shortest

def fsp_select_shortest(first, second):
    mini = min(len(first), len(second))
    if len(first) == len(second):
        # lengths are equal.
        if 'IDLE' in first:
            shortest = first
        if 'IDLE' in second:
            shortest = second
        else:
            shortest = first

    else:
        if mini == len(first):
            shortest = first
        else:
            shortest = second
    return shortest

def fsp_post(graph, start, end, path=[]):
    """faster version of find_shortest_path, because of checks in first call"""
    #print("start:", start, "end:",end)
    path = path + [start]
#    print("PATH :",path, "start",start,"end",end)
    if start == end:
        return path
    if not start in graph.keys():
       #     print("error, start node not in graph")
        return None
    shortest = None
    for node in graph[start]:
        if node not in path:
            newpath = fsp_post(graph, node, end, path)
            if newpath:
                if not shortest or len(newpath) < len(shortest):
                    shortest = newpath
    return shortest
    

