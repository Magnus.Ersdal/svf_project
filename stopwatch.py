# -*- coding: utf-8 -*-
"""
Created on Tue Mar 27 10:28:30 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
"""
import time


class Stopwatch():
    def __init__(self):
        self.starttime = time.time()
        self.endtime = 0
        self.dt = 0
        self.dts = 0
        self.dtm = 0

    def start(self):
        self.starttime = time.time()

    def stop(self):
        self.endtime = time.time()
        self.dt = self.endtime - self.starttime
        self.dtm = int(self.dt/60)
        self.dts = self.dt - (self.dtm * 60)
        print("operation completed in {a:d} minutes {b:f} seconds".format(
            a=self.dtm, b=self.dts))
        return self.dt
