# -*- coding: utf-8 -*-
"""
Created on Thu Mar 22 09:25:49 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
"""


test = {1, 2, 34, 5}
"""naming conventions from Asset svf specification, revision E"""
valid_svf_states = [
    "RESET", "IDLE", "DRSELECT", "DRCAPTURE", "DRSHIFT", "DRPAUSE",
    "DREXIT1", "DREXIT2", "DRUPDATE", "IRSELECT", "IRCAPTURE",
    "IRSHIFT", "IRPAUSE", "IREXIT1", "IREXIT2", "IRUPDATE"
]
valid_svf_end_states = ["IRPAUSE", "DRPAUSE", "RESET", "IDLE"]

valid_tdo_signal_states = ["IRSHIFT", "DRSHIFT"]

# new graph that forces 00 when IRPAUSE, DRPAUSE ::
# IDLE2, DRPAUSE2, IRPAUSE2 is faaake states. 
#tap_graph = {'RESET': ['IDLE', 'RESET'],
#             'IDLE': ['IDLE2'],
#             'IDLE': ['IDLE', 'DRSELECT'],
#             'DRSELECT': ['DRCAPTURE', 'IRSELECT'],
#             'DRCAPTURE': ['DRSHIFT', 'DREXIT1'],
#             'DRSHIFT': ['DRSHIFT', 'DREXIT1'],
#             'DREXIT1': ['DRPAUSE', 'DRUPDATE'],
#             'DRPAUSE': ['DRPAUSE2'],
#             'DRPAUSE2': ['DRPAUSE', 'DREXIT2'],
#             'DREXIT2': ['DRSHIFT', 'DRUPDATE'],
#             'DRUPDATE': ['IDLE', 'DRSELECT'],
#             'IRSELECT': ['IRCAPTURE', 'RESET'],
#             'IRCAPTURE': ['IRSHIFT', 'IREXIT1'],
#             'IRSHIFT': ['IRSHIFT', 'IREXIT1'],
#             'IREXIT1': ['IRPAUSE', 'IRUPDATE'],
#             'IRPAUSE': ['IRPAUSE2'],
#             'IRPAUSE2': ['IRPAUSE', 'IREXIT2'],
#             'IREXIT2': ['IRSHIFT', 'IRUPDATE'],
#             'IRUPDATE': ['IDLE', 'DRSELECT'],
#             'blind' : ['', '']
#             }



tap_graph = {'RESET': ['IDLE', 'RESET'],
             'IDLE': ['IDLE', 'DRSELECT'],
             'DRSELECT': ['DRCAPTURE', 'IRSELECT'],
             'DRCAPTURE': ['DRSHIFT', 'DREXIT1'],
             'DRSHIFT': ['DRSHIFT', 'DREXIT1'],
             'DREXIT1': ['DRPAUSE', 'DRUPDATE'],
             'DRPAUSE': ['DRPAUSE', 'DREXIT2'],
             'DREXIT2': ['DRSHIFT', 'DRUPDATE'],
             'DRUPDATE': ['IDLE', 'DRSELECT'],
             'IRSELECT': ['IRCAPTURE', 'RESET'],
             'IRCAPTURE': ['IRSHIFT', 'IREXIT1'],
             'IRSHIFT': ['IRSHIFT', 'IREXIT1'],
             'IREXIT1': ['IRPAUSE', 'IRUPDATE'],
             'IRPAUSE': ['IRPAUSE', 'IREXIT2'],
             'IREXIT2': ['IRSHIFT', 'IRUPDATE'],
             'IRUPDATE': ['IDLE', 'DRSELECT']
             }
