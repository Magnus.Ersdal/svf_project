# -*- coding: utf-8 -*-
"""
Created on Wed Mar 21 14:01:43 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)


"""
# small subset of svf standard commands.
#import jtag_if.jtag_ctrl as jtag_control
import jtag_if.jtag_sequencer
import jtag_if.jtag_ctrl


# tap.state_change(state)

from svf_dictionary import tap_graph, valid_svf_end_states
#from zmqsender_config import Errlvl
import settings
from memoize import Memoize
from settings import jtg_tp #jtg_seq

#from jtag_if.jtag_
#z = settings.z

def get_int_and_b_len(binstring):
    return int(binstring, 2), len(binstring)

@Memoize
def reverse_int(number, length):
#    return number
    return int(bin(number)[2:].rjust(length, '0')[::-1],2)

class jsignal(str):
    def __init__(self):
        self.D = 0
        self.MASK = ""
        self.SMASK = ""
        self.DO = ""
        self.ActualData = ""
        self.length = 0

    def get(self):
        return self.D

    def cleardata(self):
        self.D = 0
        self.length = 0
        self.DO = 0


class jbus():
    def __init__(self):
        self.HDR = jsignal()
        self.HIR = jsignal()
        self.SDR = jsignal()
        self.SIR = jsignal()
        self.TDR = jsignal()
        self.TIR = jsignal()
        self.tdovalid = jsignal()


class bussequencer():
    """Hold the state of the bus, sequence the bus, this is more jtag than SVF"""
    def __init__(self, sequencer, j_bus=None, cfg = None):#, jtag_interface=None):
        if j_bus == None:
            self.bus = jbus()
        else:
            self.bus = j_bus  
        if sequencer == None:
            print("wrong parameter to bussequencer")    
        else:
            self.jtg_seq = sequencer
        if cfg == None:
            pass
        else:
            if ("hdrlen" in cfg) and ("hirlen" in cfg): #mutually inclusive options
                print("setting JTAG header options")
                # this is the jtag chain setting
                self.setheader(int(cfg[hirlen]), int(cfg[hdrlen]))
            # TODO : trailer NOT IMPLEMENTED, because it is not in the RU hardware.
        self.ENDDR = ''
        self.ENDIR = ''

    def clearbus(self):
        self.bus = jbus()

    def setheader(self,irlength,drlength):
        self.HDR.D = 0
        self.HDR.length = drlength
        self.HIR.D = int("1" * irlength , 2)
        self.HIR.length = irlength

    def settrailer(self,irlength,drlength):
        self.TDR.D = 0
        self.TDR.length = drlength
        self.TIR.D = int("1" * irlength , 2)
        self.TIR.length = irlength 

    def scan_dr(self):
        tdi_d_a = self.reverse((self.bus.HDR.D, self.bus.HDR.length))
        tdi_d_b = self.reverse((self.bus.SDR.D, self.bus.SDR.length))
        tdi_d_c = self.reverse((self.bus.TDR.D, self.bus.TDR.length))
        tdi_d_len = self.bus.HDR.length + self.bus.SDR.length + self.bus.TDR.length
        
        tms_start, tms_start_l = get_int_and_b_len(jtg_tp.state_change('DRSHIFT'))
        tms_end, tms_end_l = get_int_and_b_len(jtg_tp.state_change(self.ENDDR))
        
        # list of tuples with (data,length)
        tms_tuples= [(tms_start, tms_start_l),(0,(tdi_d_len-1)),(tms_end , tms_end_l)]
        tdi_tuples= [(0, tms_start_l), tdi_d_a, tdi_d_b, tdi_d_c, (0,(tms_end_l-1))]
        deltal =sum([item[1] for item in tms_tuples]) - sum([item[1] for item in tdi_tuples])
        try:
            assert deltal == 0
        except ValueError:
            print("error! deltal =", deltal)  
        
        
        self.jtg_seq.tqueue(tms_tuples,tdi_tuples)

    def scan_ir(self):
        tdi_d_a = self.reverse((self.bus.HIR.D, self.bus.HIR.length))
        tdi_d_b = self.reverse((self.bus.SIR.D, self.bus.SIR.length))
        tdi_d_c = self.reverse((self.bus.TIR.D, self.bus.TIR.length))
        tdi_d_len = self.bus.HIR.length + self.bus.SIR.length + self.bus.TIR.length
        
        tms_start, tms_start_l = get_int_and_b_len(jtg_tp.state_change('IRSHIFT'))
        tms_end, tms_end_l = get_int_and_b_len(jtg_tp.state_change(self.ENDIR))
        
        # list of tuples with (data,length)
        tms_tuples= [(tms_start,tms_start_l),(0,(tdi_d_len-1)),(tms_end,tms_end_l)]
        tdi_tuples= [(0,tms_start_l), tdi_d_a, tdi_d_b, tdi_d_c, (0,(tms_end_l-1))]
        deltal =sum([item[1] for item in tms_tuples]) - sum([item[1] for item in tdi_tuples])
        try:
            assert deltal == 0
        except ValueError:
            print("error! deltal =", deltal)  
        
        
        self.jtg_seq.tqueue(tms_tuples,tdi_tuples)
#        return tms, tdi

    def set_enddr(self, enddr):
        assert enddr in valid_svf_end_states
        self.ENDDR = enddr

    def set_endir(self, endir):
        assert endir in valid_svf_end_states
        self.ENDIR = endir
    
    def reverse(self, tp):
        return reverse_int(tp[0],tp[1]), tp[1]


class svfinterpreter():
    """Interpret svf commands into something that can run on JTAG"""
    global svfcommands

    def __init__(self, sca, cfg = None):
#        self.jtg = jtag.interface()
        global jtg_c
        self.jtg_c = jtag_if.jtag_ctrl.Control(sca) 
        self.jtg_seq = jtag_if.jtag_sequencer.Seq(sca)
        self.jtg_c.insertseq(self.jtg_seq)
        self.bus = jbus()
        self.seq = bussequencer(sequencer = self.jtg_seq,
                                j_bus = self.bus,
                                cfg = cfg )#, self.jtg)
        self.func = ""

    def set_jtag_frequency(self, arg = []):
        if len(arg) != 0:
            hz = int(float(arg.pop(0)))
            self.jtg_c.setf(hz)
            pass
        else:
            # set max freq
            self.jtg_c.setfmax()
            pass

    def set_state(self, arg = None):  # arg not optional.
        """Force the JTAG bus to a stable state"""
        if arg is None:
            assert arg != None
            print("error, set_state() requires at least one argument")
        else:  # send wanted state to jtag interface
            if len(arg) > 1:
                assert False # not implemented
            else:
                s, s_l = get_int_and_b_len(jtg_tp.state_change(arg[0]))
            self.jtg_seq.tqueue([(s,s_l)],[(0,s_l)])

    def run_test(self, arg = None):
        # THIS function is INCOMPLETE. it only deals with two cases.
        # change state, then wait
        arg0 = arg.pop(0)
        time = 0
        if arg0 in tap_graph:  # is arg a tap state?
            #            print("arg is :", arg)
            self.set_state([arg0])
            waitstate = arg0
            time = float(arg.pop(0))
        else:  # if it is not a state is is either run count or min_time
            assert False ## TODO not implemented
            time = float(arg0)
        indicator = arg.pop(0)
#        tms_s = jtg_tp.state_change(waitstate)
#        tdi_s = "0"*len(tms_s)
        self.jtg_c.wait(time, indicator, waitstate)

    def endir(self, arg= None):  # establish the end state for scan operation
        self.seq.set_endir(arg.pop(0))

    def enddr(self, arg= None):  # establish the end state for scan operation
        self.seq.set_enddr(arg.pop(0))

    def hdr(self, arg= None):
        self.data_parse(arg, 'HDR', self.bus.HDR)

    def hir(self, arg= None):
        self.data_parse(arg, 'HIR', self.bus.HIR)

    def sdr(self, arg= None):
        self.data_parse(arg, 'SDR', self.bus.SDR)
        self.seq.scan_dr()


    def sir(self, arg= None):
        self.data_parse(arg, 'SIR', self.bus.SIR)
        self.seq.scan_ir()


    def tdr(self, arg= None):
        self.data_parse(arg, 'TDR', self.bus.TDR)

    def tir(self, arg= None):
        self.data_parse(arg, 'TIR', self.bus.TIR)

    def data_parse(self, arg= None, mode='', sig=None):
        sig.cleardata() # might introduce bugs !! 
        length = int(arg.pop(0))
        #[[TDI, 123], [TDO, 123]]
        if len(arg) != 0:
            self._data_sub_parse(arg, None, mode, sig, length)

    def _data_sub_parse(self, arg, sequence=None, mode='', sig=None, length=0):
        """parses an argument into the correct signal bin"""
        #print("datasubparse: {}".format(arg))
        # the alternatives for "sig" variable are self.bus.TDI
        if isinstance(arg, str):
            sig.length = length
            if arg == 'TDI':
                sig.D = sequence #int(bin(sequence)[2:][::-1], 2)
            elif arg == 'TDO':
                sig.DO = sequence
            elif arg == 'MASK':
                sig.MASK = sequence
            elif arg == 'SMASK':
                sig.SMASK = sequence
            else:
                print("Error", arg, "not valid for sdr")
                print(arg,sequence,mode,sig,length)
                assert False
        elif isinstance(arg, list):
            for item in arg:
                bsequence = int(item[1], 16) #
                self._data_sub_parse(item[0], bsequence, mode, sig, length=length)

    def dummy(self, arg=None):
        pass
#        print("NOT implemented (yet?)")

    def caller(self, arg = []):
        '''call the correct function'''
        if arg == []:
            print("caller got empty argument")
        else:
            svfcase = arg.pop(0)
            if svfcase in svfcommands:
             # call function. this is the alternative to switch/case in python
                self.func = svfcommands[svfcase]
                self.func(self, arg)
            else:
                print("Error, ", svfcase, "function not in command list")


    def end(self):
        self.jtg_seq.end_clean()
        print(self.jtg_seq.get_tdo_bits_list())
        return self.jtg_seq.output
#        self.jtg.sca.ch.__del__() # safe shutdown.
        
    svfcommands = {
        'FREQUENCY': set_jtag_frequency,
        'ENDDR': enddr,
        'ENDIR': endir,
        'HDR': hdr,
        'HIR': hir,
        'RUNTEST': run_test,  # run_test,
        'SDR': sdr,
        'SIR': sir,
        'STATE': set_state,
        'TDR': tdr,
        'TIR': tir}
    """ how to call the functions: 
    func = functions[value]
    func()
    """
