# -*- coding: utf-8 -*-
"""
Created on Tue Mar 20 15:03:19 2018

@author: Magnus Rentsch Ersdal, magnus.ersdal@uib.no
"""
import time

# mystring is for testign purposes.

# call: li = separate_svf_cmds(remove_svf_comments("p.svf"))

#"RUNTEST IDLE 1 TCK;\nSDR 32 TDI(00000000) TDO(063261CF) MASK(07FFFFFF);\nENDIR IRPAUSE;\nENDDR DRPAUSE;\nSIR 8 TDI(01);\nSDR 1056 TDI(9249249249249249249249249249249249249249249249249249249249249249249\n    2492492492492492492492492492492492492492492492492492492492492492492492492492\n    4924924924924924924924924924924924924924924924924924924924924924924924924924\n    924924924924924924924924924924924924924924924);\n    STATE IDLE;\nRUNTEST IDLE 1 TCK;"
mystring = "SDR 1056 TDI(9249249249249249249249249249249249249249249249249249249249249249249\n    2492492492492492492492492492492492492492492492492492492492492492492492492492\n    4924924924924924924924924924924924924924924924924924924924924924924924924924\n    924924924924924924924924924924924924924924924);\n    STATE IDLE;\nRUNTEST IDLE 1 TCK;"
#"SDR 18 TDI(00000) TDO(30000) MASK(30000);\nENDIR IRPAUSE;"
#"SDR 1056 TDI(9249249249249249249249249249249249249249249249249249249249249249249\n    2492492492492492492492492492492492492492492492492492492492492492492492492492\n    4924924924924924924924924924924924924924924924924924924924924924924924924924\n    924924924924924924924924924924924924924924924);\n    STATE IDLE;\nRUNTEST IDLE 1 TCK;"


def get_str_arg(s):
    """get anything between parentheses, else return the string"""
    return s[s.find("(")+1:s.find(")")]  # https://stackoverflow.com/users/185175/tkerwin


def get_str_arg_plus(s):
    procs = get_str_arg(s)
    if procs != s[0:-1]:
        command = s[0:s.find("(")]
        procs = procs.replace(" ", "")
        procs = procs.replace("\n", "")
        arg = procs
        return [command, arg]
    return s


items2 = []
items4 = []


def separate_svf_cmds(s):
    """Separate svf commands into lists in a list, and commands with parameters 
    into lists in those lists, to make it easy to parse downstream
    Usage: mylist = separate_svf_cmds(string)"""
    items2 = []
    items4 = []
    items2 = s.split(";")
    for item in items2:
        item = item.replace("  ", "")
        item = item.replace("\n", "")
        item = item.replace("\r", "") # python 2 compat
#    items4.append(item.rsplit())
        ilist = []
        for m in item.rsplit():
            ilist.append(get_str_arg_plus(m))
        items4.append(ilist)
    return items4


filestring = ""


def remove_svf_comments(filename):
    """Remove any comment lines from a svf file and build a string"""
    filestring = ""
    f = open(filename)
    for line in f.readlines():
        if(line.find("!") == -1):  # no comment found
            filestring = filestring + line

    return filestring


def read_svf(filename):
    """Read svf filename into parsable list type format"""
    print("this operation takes about 1 minute per million lines of commands")
    starttime = time.time()
    biglist = separate_svf_cmds(remove_svf_comments(filename))
    endtime = time.time()
    dt = endtime - starttime
    dtm = int(dt/60)
    dts = dt - (dtm * 60)
    print("operation completed in {a:d} minutes {b:f} seconds".format(
        a=dtm, b=dts))
    return biglist

