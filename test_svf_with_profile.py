# -*- coding: utf-8 -*-
"""
Created on Mon Mar 26 23:43:04 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)


"""
from __future__ import print_function

print("start!")
import svf_intepreter as interpreter
from stopwatch import Stopwatch
from svf_separate_commands import read_svf
from hwdriver.SCA import Sca
import pstats



#settings.init()

def space():
    print("====================================================================")


def littlespace():
    print("------")

def test():
    listone = read_svf("program_pa3.svf") #("idcode.svf")#("../p_400klines.svf")#("../p_reduced.svf")
    totlen = len(listone)
    print("length of data list:",totlen)
    if totlen < 20:
        print(listone)
    totpc = totlen / 100
    if totpc < 1:
        totpc = 1
    sw1 = Stopwatch()
    
    
    id_card = "33554941"
    gbt_ch = 0
    board = "CRU"
    sca = Sca(id_card, 2, gbt_ch, board)
    #def __init__(self, pcie_id, bar_ch, gbt_ch, board, debug=None,logger = None):
    
    try:
        i = 0
        svf = interpreter.svfinterpreter(sca)
        space()
        for test in listone:
            svf.caller(test)
            i +=1
        svf.end()
    except KeyboardInterrupt:
        print("----- C-C detected! -----")
    finally:
        sw1.stop()
        print("ran {} tests".format(i))
        

def profile_test():
    import cProfile
    cProfile.run('test()', "profiledata")

    
if __name__== '__main__':
    profile_test()
    p = pstats.Stats('profiledata')
    p.strip_dirs()
    p.sort_stats('cumulative').print_stats(10)
    p.sort_stats('tottime').print_stats(10)
    
    
