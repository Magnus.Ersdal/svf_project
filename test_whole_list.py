# -*- coding: utf-8 -*-
"""
Created on Mon Mar 26 23:43:04 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
"""

"""requires a pre-load of the commands in a variable called "li" SE"""
"""
without print: operation completed in 3 minutes 10.838241 seconds
"""
print("start!")

import svf_intepreter as interpreter
from stopwatch import Stopwatch


def space():
    print("====================================================================")


def littlespace():
    print("------")


listone = list(li)
listtwo = list(listone)

sw1 = Stopwatch()

svf = interpreter.svfinterpreter()
space()
for test in listone:
 #   print("testing", test)
    #    littlespace()
    svf.caller(test)
#    space()
space()
sw1.stop()
