# -*- coding: utf-8 -*-
"""
Created on Mon Mar 26 23:43:04 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)

full read / interpret :L operation completed in 8 minutes 8.541882 seconds
without print: operation completed in 3 minutes 10.838241 seconds
5 secs
6 minutes 29.227718 seconds
before move:
    operation completed in 4 minutes 41.998992 seconds
after move1:
    operation completed in 6 minutes 32.186195 seconds
after move2:
    operation completed in 5 minutes 28.346667 seconds
after move3:
    operation completed in 4 minutes 59.913497 seconds

200klines
MEDIUM logging :
    operation completed in 0 minutes 27.715771 seconds
LOW logging
"""
from __future__ import print_function

print("start!")
import svf_intepreter as interpreter
from stopwatch import Stopwatch
from svf_separate_commands import read_svf
from hwdriver.SCA import Sca
import copy
import sys


#settings.init()
def space():
    print("====================================================================")


def littlespace():
    print("------")

listone = read_svf("idcode.svf")#("program_pa3.svf") #("idcode.svf")#("../p_400klines.svf")#("../p_reduced.svf")
# operation completed in 4 minutes 27.190409 seconds for 200klines with delays. 
# operation completed in 4 minutes 56.811993 seconds for 400klines with delays.
# operation completed in 8 minutes 8.541882 seconds
# operation completed in 2 minutes 30.999758 seconds with no delays.. .
#listone = copy.deepcopy(li)
totlen = len(listone)
totpc = totlen / 100
if totpc < 1:
    totpc = 1
sw1 = Stopwatch()


id_card = "33554941"
gbt_ch = 0
board = "CRU"
sca = Sca(id_card, 2, gbt_ch, board)
#def __init__(self, pcie_id, bar_ch, gbt_ch, board, debug=None,logger = None):

try:
    i = 0
    svf = interpreter.svfinterpreter(sca)
    space()
    for test in listone:
        #print(test)
        svf.caller(test)
        i += 1
    li = svf.end()
except KeyboardInterrupt:
    print("----- C-C detected! -----")
finally:
    sw1.stop()
    print("ran {} tests".format(i))
