import sys

sys.setrecursionlimit(int(1.744e4))

def f(x=2):
    try:f(x+1)
    except:print(x)


print("running recursion limit checker with limit", sys.getrecursionlimit())
f()
