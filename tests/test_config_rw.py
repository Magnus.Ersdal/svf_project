"""
author = magnus.ersdal@uib.no
"""

filename = "defaults.cfg"
with open(filename,'r') as cfgfile:
    import sys
    if sys.version_info >= (3,0):
        # we are in python 3xx 
        import configparser as cp
        config = cp.ConfigParser()
        config.read_file(cfgfile)
    else:
        # python2 :(
        print("Python 2. Upgrade required by 2020")
        import ConfigParser as cp
        config = cp.ConfigParser()
        config.readfp(cfgfile)


sect = ['CRU']*2 + ['RU'] + ['FILE']
opt = ['id_card','board','gbt_ch','svf_filename']
variables = {}
#variables = {
    #"id_card":"-1",
    #"board":"-1",
    #"gbt_ch":-1,
    #"svf_filename":"-1"
    #}

print("cfg file read")
try:
    for section, option in zip(sect,opt):
        print(section,option, config.get(section,option))
        variables[option] = config.get(section,option)
except (NameError, cp.NoOptionError) as e:
    print("Name/NoOptionError !, name \'{}\' is not defined.\
 Please check the config file".format(option))
    print e.message
    raise

print("cfg dict:")
print(variables)

