# -*- coding: utf-8 -*-
"""
Created on Wed Apr 18 10:18:04 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
"""

from stopwatch import Stopwatch
import random
import string
from itertools import repeat
#        self.tdi_bits += tdi_bits
#        self.tdi_bits = "".join([self.tdi_bits,tms_bits])

def concat(a,b):
    a += b
    return a

def join(a,b):
    return "".join([a,b])

LENGTH = int(5e6)
N = 10000

string_a = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))
string_b = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))

try:
    sw1 = Stopwatch()
    for x in range(LENGTH):
        concat(string_a, string_b)
    sw1.stop()
    
    sw2 = Stopwatch()
    for x in range(LENGTH):
        join(string_a,string_b)
    sw2.stop()
    
    sw3 = Stopwatch()
    _ = [a+b for a,b in list(zip(repeat(string_a), repeat(string_b, LENGTH)))]
    sw3.stop()
except KeyboardInterrupt:
    print("C-C!!")
        