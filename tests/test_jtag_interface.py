# -*- coding: utf-8 -*-
"""
Created on Wed Mar 21 17:26:00 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
"""

import jtag_interface as jtag
from SCARegs import SCARegs


def space():
    print("====================================================================")


space()
jtg = jtag.interface()
space()
jtg.setfmax()
space()

print("Current state:", jtg.tap.current_state)
jtg.state_change('IRUPDATE')
print("Current state:", jtg.tap.current_state)
jtg.state_change('IRCAPTURE')
print("Current state:", jtg.tap.current_state)

space()
print("testing \"safewrite\" function")
newlength = 20
jtg.safewrite(SCARegs['JTAG_W_CTRL'], SCARegs['JTAG_R_CTRL'], newlength, 0x3f)
space()

longlong_str = "1" * 128 + "01" * 64 + "1100" * \
    32  # 128 * 3 = 32  *4 * 3 = 12 * 32bit writes
jtg.test_tms_write(longlong_str)
space()
print("SCA final \"Configuration\":::")
# "{0:b}".format(int(hexstring,16))
print([hex(x) for x in jtg.sca.ch.registers.keys()])
print([hex(x) for x in jtg.sca.ch.registers.values()])
print("\nWrite total:", jtg.sca.ch.writecount,
      "read total:", jtg.sca.ch.readcount)
