# -*- coding: utf-8 -*-
"""
Created on Tue Mar 27 12:13:55 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
"""

from logbook import Logger, FileHandler
import stopwatch
import sys
f = "logfile2.txt"
FileHandler(f).push_application()
log = Logger("My first logger")
log.warn("this is better than stdlib?")

log.info("some info")