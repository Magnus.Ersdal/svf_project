# -*- coding: utf-8 -*-
"""
Created on Tue Mar 27 12:43:10 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
"""

from logbook.queues import ZeroMQSubscriber
from logbook import StreamHandler
from logbook import Logger
import sys

import time
subscriber = ZeroMQSubscriber('tcp://127.0.0.1:5003')
log_handler = StreamHandler(sys.stdout)
log = Logger("a test receiver")

print("program begin")
with log_handler.applicationbound():
    try:
        i = 0
        while True:
            i += 1
            record = subscriber.recv()         
            log.handle(record)
            time.sleep(0.5)
    except KeyboardInterrupt:
        print("program end")