# -*- coding: utf-8 -*-
"""
Created on Tue Mar 27 12:41:44 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
test logbook multiprocessing
"""


from logbook.queues import ZeroMQHandler
from logbook import info
from logbook import warn
import time
#queue = Queue(-1)
#handler = MultiProcessingHandler(queue)

handler = ZeroMQHandler('tcp://127.0.0.1:5005')
#handler.export_record
i = 0
try:
    while True:
        with handler.applicationbound():
            info("sent test", i)
            print("\r", end='')
            print(i, end='', flush=True)
            i += 1
            time.sleep(0.7)
except KeyboardInterrupt:
    print("C-C !!!, program exit")
handler.close()