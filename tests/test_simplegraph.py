# -*- coding: utf-8 -*-
"""
Created on Thu Mar 22 14:12:39 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
"""

from simplegraph import find_shortest_path as fsp
from svf_dictionary import tap_graph

testvector = ['DRSELECT', 'DRCAPTURE', 'DREXIT1', 'DREXIT2', 'DRUPDATE',
              'IRSELECT', 'IRCAPTURE', 'IREXIT1', 'IREXIT2', 'IRUPDATE']
for state in testvector:
    path = fsp(tap_graph, state, state)
    print(path)

    # from the standard, possibly modified to correspond to the actual tap graph
svf_standard_transition_table = [
    ['RESET', 'RESET', ['RESET']],
    ['RESET', 'IDLE', ['RESET', 'IDLE']],
    ['RESET', 'DRPAUSE', ['RESET', 'IDLE', 'DRSELECT',
                          'DRCAPTURE', 'DREXIT1', 'DRPAUSE']],
    ['RESET', 'IRPAUSE', ['RESET', 'IDLE', 'DRSELECT', 'IRSELECT',
                          'IRCAPTURE', 'IREXIT1', 'IRPAUSE']],  # this is different than the table
    ['IDLE', 'RESET', ['IDLE', 'DRSELECT', 'IRSELECT', 'RESET']],
    ####
    ['IDLE', 'IDLE', ['IDLE']],
    ['IDLE', 'DRPAUSE', ['IDLE', 'DRSELECT', 'DRCAPTURE', 'DREXIT1', 'DRPAUSE']],
    ['IDLE', 'IRPAUSE', ['IDLE', 'DRSELECT',
                         'IRSELECT', 'IRCAPTURE', 'IREXIT1', 'IRPAUSE']],
    ['DRPAUSE', 'RESET', ['DRPAUSE', 'DREXIT2',
                          'DRUPDATE', 'DRSELECT', 'IRSELECT', 'RESET']],
    ['DRPAUSE', 'IDLE', ['DRPAUSE', 'DREXIT2', 'DRUPDATE', 'IDLE']],
    ####
    # DRPAUSE - DRPAUSE example removed
    ['DRPAUSE', 'IRPAUSE', ['DRPAUSE', 'DREXIT2', 'DRUPDATE',
                            'DRSELECT', 'IRSELECT', 'IRCAPTURE', 'IREXIT1', 'IRPAUSE']],
]

print()
print("testing standard vs generator paths")
for item in svf_standard_transition_table:
    errorcount = 0
    startstate = item[0]
    endstate = item[1]
    path = item[2]
    genpath = fsp(tap_graph, startstate, endstate)
    if genpath != path:
        print("found possible error in path", startstate, "to", endstate)
        print(path, "!=", genpath)
        errorcount = errorcount + 1
if errorcount:
    print("there was", errorcount, "unexpected errors")
else:
    print("test did not find any path errors")
