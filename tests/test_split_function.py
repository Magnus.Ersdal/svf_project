# -*- coding: utf-8 -*-
"""
Created on Fri Mar 23 11:12:43 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
"""


def split(vect: str, chunksize: int):
    nwords = ceil(len(vect)/chunksize)
    # vect = [ vect[i:i+chunksize] for i in range(0, nwords, chunksize) ] # https://stackoverflow.com/users/417685/alexander
    vect = [vect[i:i+chunksize] for i in range(0, nwords*chunksize, chunksize)]
    return vect


print(split("11110000", 4))
