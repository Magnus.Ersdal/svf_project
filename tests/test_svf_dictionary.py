# -*- coding: utf-8 -*-
"""
Created on Thu Mar 22 10:05:04 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
"""
from itertools import combinations

from simplegraph import find_all_paths, find_shortest_path

from svf_dictionary import tap_graph, valid_svf_states, valid_svf_end_states

# find_all_paths(tap_graph)


def issubset(large, small):
    return False not in [key in large for key in small]


# False not in [key in valid_svf_states for key in valid_svf_end_states] ## is subset..
assert issubset(valid_svf_states, valid_svf_end_states)


def construct_tms_test(tapstates: list):
    """make binary TMS signal from state transition list"""
    tms_string = ""
    # for multiple paths
    if len(tapstates) > 1:
        print("finding binary combo for ", tapstates)
        for idx, state in enumerate(tapstates):
            if idx < (len(tapstates) - 1):
                nextstate = tapstates[idx+1]
                tms_string = tms_string + \
                    str(tap_graph[state].index(nextstate))

    if len(tapstates) == 1:
        state = tapstates[0]
        tms_string = tms_string + str(tap_graph[state].index(state))
    return tms_string


for combo in combinations(valid_svf_states, 2):
    print(combo)
    print(find_shortest_path(tap_graph, combo[0], combo[1]))
    print(construct_tms_test(find_shortest_path(
        tap_graph, combo[0], combo[1])))
    print(find_shortest_path(tap_graph, combo[1], combo[0]))
    print(construct_tms_test(find_shortest_path(
        tap_graph, combo[1], combo[0])))
print("single item tests")
singletests = valid_svf_states
for state in singletests:
    print(state)
    path = find_shortest_path(tap_graph, state, state)
    print(construct_tms_test(path))
print()
