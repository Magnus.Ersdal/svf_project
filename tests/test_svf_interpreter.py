# -*- coding: utf-8 -*-
"""
Created on Wed Mar 21 15:49:30 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
"""

import svf_intepreter as interpreter
from settings import z
from hwdriver.SCA import Sca

def space():
    """prints this"""
    print("====================================================================")


def littlespace():
    """prints this"""
    print("------")
    
    
id_card = 1
gbt_ch = 0
board = "CRU"
sca = Sca(id_card, 2, gbt_ch, board, logger = z)


testvector = [
    ['FREQUENCY', '4E6', 'HZ'],
    ['FREQUENCY'],
    ['STATE', 'RESET'],
    [999],
    [],
    ['RUNTEST', 'IDLE', '5', 'TCK'],  # 50%
    ['ENDIR', 'IRPAUSE'],
    ['ENDDR', 'DRPAUSE'],
    ['SIR', '8', ['TDI', '0F']],
    ['SDR', '32', ['TDI', 'BEEFCAFE']],
    ['SDR', '32', ['TDI', '00000000'], [
        'TDO', '063261CF'], ['MASK', '07FFFFFF']],
    ['SDR', '1056', ['TDI', '924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924924']],
    ['STATE', 'IDLE'],
]
svf = interpreter.svfinterpreter(logger =z , sca=sca)
space()
for test in testvector:
 #   print("testing", test)
 #   littlespace()
    svf.caller(test)
  #  space()
