# -*- coding: utf-8 -*-
"""
Created on Wed Mar 21 15:49:30 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
"""

import svf_intepreter as interpreter
from stopwatch import Stopwatch
import copy

def space():
    print("====================================================================")


def littlespace():
    print("------")

# with logging in libReadoutCard.py ::: 120 seconds / 100001 first elements
# without logging in libReadoutCard.py :: 
# without logging import in libReadoutCard.py :: 7.774683 s / 100001 first elements
"""
with zmq logging: 
    0:1001 : 4.15 s
    0:10001: 30.43 s
with zmq receiver running in separate thread:
    0:1001 :
without:
    0:1001 : 0.072182 s
    0:10001: 0.793108 s
"""
testvector = []
testvector = copy.deepcopy(li[0:1001])
svf = interpreter.svfinterpreter()
try:
    space()
    sw1 = Stopwatch()
    for test in testvector:
     #   print("testing", test)
     #   littlespace()
        svf.caller(test)
      #  space()
finally:
    svf.end()
sw1.stop()