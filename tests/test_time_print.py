# -*- coding: utf-8 -*-
"""
Created on Tue Mar 27 09:32:22 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
"""
print("start!")

import time
import os
import sys


class HiddenPrints:
    def __enter__(self):
        self._original_stdout = sys.stdout
        sys.stdout = None

    def __exit__(self, exc_type, exc_val, exc_tb):
        sys.stdout = self._original_stdout


def space():
    print("====================================================================")


def littlespace():
    print("------")


starttime = time.time()
space()
# 2 minutes 48.122 seconds = 168.122 seconds for 2e8 calls without print 166.122/2e8 = 8.3e-7 per loop
#
with HiddenPrints():
    for test in range(int(2e3)):
        print("this is some text for test number :", test)
        littlespace()
        print("more text")
        space()
# 10.7 seconds for 2e4 calls with print 10.7/2e4 = 5.3e-4  per loop. factor 1000 difference..
for test in range(int(2e4)):
    print("this is some text for test number :", test)
    littlespace()
    print("more text")
    space()


endtime = time.time()
dt = endtime - starttime
dtm = int(dt/60)
dts = dt - (dtm * 60)
print("operation completed in {a:d} minutes {b:f} seconds".format(
    a=dtm, b=dts))
