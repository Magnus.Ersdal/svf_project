# -*- coding: utf-8 -*-
"""
Created on Tue Apr  3 15:17:13 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
"""

import time
from stopwatch import Stopwatch
TOTAL_T_CNTS = int(10)
TOTAL_T_TIME = 5.0
sleeptime = 1/TOTAL_T_CNTS * TOTAL_T_TIME

try:
    sw = Stopwatch()
    for i in range(TOTAL_T_CNTS):
        time.sleep(sleeptime)
except KeyboardInterrupt:    
    pass
finally:
    sw.stop()
""""   
data = [(10, 5.004325), 
        (1e3, 5.37), 
        (1e4, 14.018404)]
#overhead = [(n, oh) for n, oh = TOTAL_T_TIME - y in data]

import matplotlib.pyplot as plt
overhead =[]
for x,y in data:
    oh = (y -TOTAL_T_TIME)/TOTAL_T_TIME* 100
    overhead.append((x, oh))
    
plt.plot(*zip(*overhead))
plt.show()
""""