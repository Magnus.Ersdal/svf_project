# -*- coding: utf-8 -*-
"""
Created on Thu Apr  5 10:10:19 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
"""

from logcomponents.zmqsender import Sender
from stopwatch import Stopwatch

z = Sender()
NUM = int(1e6)

"""approximately 2 micro seconds execution time for a log.
   vs a bit less than 1 micro second execution time for 
"""
sw1 = Stopwatch()
for i in range(NUM):
    z.log("some text {}".format(i))
sw1.stop()


sw2 = Stopwatch()
for i in range(NUM):
    _ = "some text {}".format(i)
sw2.stop()

sw3 = Stopwatch()
for i in range(NUM):
    z.log("a")
sw3.stop()

testli = []
sw4 = Stopwatch()
for i in range(NUM):
    testli.append("some text {}".format(i))
sw4.stop()