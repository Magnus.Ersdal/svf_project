# -*- coding: utf-8 -*-
"""
Created on Wed Apr  4 12:13:39 2018

@author: Magnus Rentsch Ersdal (magnus.ersdal@uib.no)
"""



from enum import Enum

class Errlvl(Enum):
    LOW = 0
    MEDIUM = 1
    HIGH = 2
    CRITICAL = 3
    INFO = 99
    DEBUG0 = 0
    DEBUG1 = 1
    DEBUG2 = 2
    DEBUG3 = 3
    
severity_report = Errlvl.DEBUG2    
    
def ecompare(report,actual):
    return report.value <= actual.value